const mongoose = require("mongoose");
const Usuario = mongoose.model("Usuario");

module.exports.crearUsuario = function (req, res) {
	let nuevoUsuario = new Usuario();
	nuevoUsuario.nombre = req.body.nombre;
	nuevoUsuario.apellidos = req.body.apellidos;
	nuevoUsuario.telefono = req.body.telefono;
	nuevoUsuario.correo_electronico = req.body.correo_electronico;
	nuevoUsuario.categoria = req.body.categoria;
	nuevoUsuario.save(function (err) {
		if (err) {
			res.json({ "mensaje": "Ha ocurrido un error" });
		} else {
			res.json({ "mensaje": "El usuario ha sido añadido exitosamente" })
		}
	});
};

module.exports.editarUsuario = function (req, res) {
	Usuario.findById(req.params.id, function (err, usuario) {
		if (err) {
			res.json({ "mensaje": "El usuario no ha sido encontrado" });
		} else {
			usuario.nombre = req.body.nombre;
			usuario.apellidos = req.body.apellidos;
			usuario.telefono = req.body.telefono;
			usuario.correo_electronico = req.body.correo_electronico;
			usuario.categoria = req.body.categoria;
			usuario.save(function (err) {
				if (err) {
					res.json({ "mensaje": "Ha ocurrido un error al ingresar el usuario" });
				} else {
					res.json({ "mensaje": "El usuario a sido editado exitosamente" });
				}
			});
		}
	});
};

module.exports.eliminarUsuario = function (req, res) {
	Usuario.findByIdAndRemove(req.params.id, function (err, usuario) {
		if (err) {
			res.json({ "mensaje": "El usuario no ha sido encontrado" });
		} else {
			console.log('USUARIO -> ' + usuario);
			res.json({ "mensaje": "El usuario ha sido eliminado exitosamente" });
		}
	});
};

module.exports.obtenerUsuarios = function (req, res) {
	Usuario.find({}, function (err, usuarios) {
		if (err) {
			res.json({ "mensaje": "Ha ocurrido un error con el listado de usuarios" });
		} else {
			res.json({ "usuarios": usuarios });
		}
	});
};

module.exports.detalleUsuario = function (req, res) {
	Usuario.findById(req.params.id, function (err, usuario) {
		if (err) {
			res.json({ "mensaje": "Ha ocurrido un error al buscar el usuario" });
		} else {
			res.json({ "usuario": usuario });
		}
	});
};
