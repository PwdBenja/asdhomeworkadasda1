const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let usuarioSchema = new Schema({
	nombre: {
		type: String,
	},
	password: {
		type: String
	},
}, {
	timestamps: true
});

mongoose.model("Usuario", usuarioSchema);
