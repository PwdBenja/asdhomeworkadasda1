const express = require("express");
const router = express.Router();

const usuario = require("./controllers/usuario.js");

router.post("/usuario/crear", usuario.crearUsuario);
router.patch("/usuario/editar/:id", usuario.editarUsuario);
router.delete("/usuario/eliminar/:id", usuario.eliminarUsuario);
router.get("/usuario", usuario.obtenerUsuarios);
router.get("/usuario/ver/:id", usuario.detalleUsuario);

module.exports = router;
