const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

require('./api/models/index');

const app = express();
const port = 3000;

const api = require("./api/rutas");

mongoose.Promise = global.Promise;
mongoose.set('debug', true);
mongoose.connect('mongodb://127.0.0.1:27017/tds', { useNewUrlParser: true, useUnifiedTopology: true });

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use("/api/v1", api);
app.listen(port, () => {
	console.log("SERVIDOR OPERATIVO");
});

module.exports = app;