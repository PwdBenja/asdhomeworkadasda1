import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable()
export class UserService {
    apiURL = environment.apiURL;

    constructor(
        private httpClient: HttpClient
    ) { }

    crearUsuario(usuario: any) {
        return this.httpClient.post(this.apiURL + "usuario/crear", usuario);
    }

    editarUsuario(usuario: any) {
        return this.httpClient.patch(this.apiURL + "usuario/editar/" + usuario._id, usuario);
    }

    eliminarUsuario(usuario: any) {
        return this.httpClient.delete(this.apiURL + "usuario/eliminar/" + usuario._id);
    }

    obtenerUsuarios() {
        return this.httpClient.get(this.apiURL + "usuario");
    }

    detalleUsuario(id: any) {
        return this.httpClient.get(this.apiURL + "usuario/ver/" + id);
    }

}
