import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent {
  usuarios: any;
  usuario: any = {
    "nombre": "",
    "password": ""
  };
  categorias: any;
  mensaje: any;
  editando = false;

  constructor(
    private userService: UserService,
    private router: Router
  ) {
    this.cargarUsuarios();
  }

  enviarFormulario() {
    this.userService.crearUsuario(this.usuario).subscribe(
      (data: any) => {
        this.mensaje = data.mensaje;
        this.cargarUsuarios();
        this.usuario = {
          "nombre": "",
          "password": ""
        };
      },
      (error: any) => {
        this.mensaje = error;
      }
    )
  }

  cargarUsuarios() {
    this.userService.obtenerUsuarios().subscribe(
      (data: any) => {
        this.usuarios = data.usuarios;
      },
      (error: any) => {
        this.mensaje = error;
      }
    )
  }

  eliminarUsuario(usuario: any) {
    var alerta = confirm('El usuario sera eliminado y no se puede deshacer ¿Desea continuar?');
    if (alerta) {
      this.userService.eliminarUsuario(usuario).subscribe(
        (data: any) => {
          this.mensaje = data.mensaje;
          this.cargarUsuarios();
        },
        (error: any) => {
          this.mensaje = error;
        }
      )
    }
  }

  cargarUsuario(usuario: any) {
    this.editando = true;
    this.usuario = {
      "_id": usuario["_id"],
      "nombre": usuario["nombre"],
      "password": usuario["password"]
    };
  }

  editarUsuario(usuario: any) {
    this.userService.editarUsuario(usuario).subscribe(
      (data: any) => {
        this.mensaje = data.mensaje;
        this.cargarUsuarios();
        this.usuario = {
          "nombre": "",
          "password": ""
        };
        this.editando = false;
      },
      (error: any) => {
        this.mensaje = error;
      }
    )
  }

  verUsuario(usuario: any) {
    this.router.navigate(['/ver'], { queryParams: { id: usuario._id } });
  }
}
