import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { UsuariosRoutingModule } from './usuarios-routing.module';
import { UsuariosComponent } from './usuarios.component';

@NgModule({
    imports: [
        UsuariosRoutingModule,
        CommonModule,
        FormsModule
    ],
    declarations: [UsuariosComponent]
})
export class UsuariosModule { }