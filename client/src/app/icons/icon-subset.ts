import {
  cilApplicationsSettings,
  cilMenu,
  cilSpeedometer,
  cilGroup
} from '@coreui/icons';

export const iconSubset = {
  cilApplicationsSettings,
  cilMenu,
  cilSpeedometer,
  cilGroup
};

export enum IconSubset {
  cilApplicationsSettings = 'cilApplicationsSettings',
  cilMenu = 'cilMenu',
  cilSpeedometer = 'cilSpeedometer',
  cilGroup = 'cilGroup'
}
