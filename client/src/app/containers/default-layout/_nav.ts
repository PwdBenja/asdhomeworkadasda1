import { INavData } from '@coreui/angular-pro';

export const navItems: INavData[] = [
  {
    name: $localize`Dashboard`,
    url: '/dashboard',
    iconComponent: { name: 'cil-speedometer' },
  },
  {
    name: 'Usuarios',
    url: '/usuarios',
    iconComponent: { name: 'cil-group' },
  }
];
